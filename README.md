# Laravel Simple Admin Package

Laravel Simple Admin Package lets you have a simple admin panel for your applications. everything defines in config file and may be customize with overriding methods in model classes.

## Installation

Use the package manager [composer](https://getcomposer.org/) to install simple admin package.

```bash
composer require yeknava/simple-admin
```

## Usage
Run this command in your terminal:

```bash
php artisan vendor:publish
```

## Config
```php
<?php

use Yeknava\SimpleAdmin\SimpleAdminBasicMiddleware;

return [
    /*
     * route's base path that will be used for package specific routes.
     * (use 'php artisan route:list' to find out all paths)
     */
    'route_base_path' => '/simple-admin',

    /*
     * middlewares that admin needs to pass before access to panel.
     * if you don't have any login process that admin may to use it to login first,
     * SimpleAdminBasicMiddleware will provide basics auth functionality. 
     */
    'middlewares' => [SimpleAdminBasicMiddleware::class, 'throttle:10,1'],

    /*
     * if you are using SimpleAdminBasicMiddleware middleware and you set
     * use_login_method config to false, you need to provide default
     * username and password to login. 
     */
    'default_username' => env('SA_USERNAME', 'username'),
    'default_password' => env('SA_PASSWORD', 'secret'),

    /*
     * if you need to use login_method option set this true
     */
    'use_login_method' => false,

    /*
     * you may check username and password with database here, return true
     * if username and password fields are match with user.
     */
    'login_method' => function(string $username, string $password) : bool {
        return true;
    },

    /*
     * admin panel pages:
     */
    'pages' => [
        'users' => [
            'path' => '/users',
            'title' => 'Users',
            'create' => true,
            'edit' => true,
            'delete' => true,
            'view' => null,
            
            /*
             * content is model class, if you need customize any fields or
             * process you need to use SimpleAdminTrait in your model class
             */
            'content' => \App\User::class,
        ],
    ]
];

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.



## License
[MIT](https://choosealicense.com/licenses/mit/)
