<?php

namespace Yeknava\SimpleAdmin\Sample;

use Exception;
use Yeknava\SimpleAdmin\SimpleAdminServiceInterface;

class SimpleAdminSampleService implements SimpleAdminServiceInterface {
    public function login (string $username, string $password) : bool {
        $defaultUsername = config('simple-admin.default_username');
        $defaultPassword = config('simple-admin.default_password');

        if(empty($defaultUsername) || empty($defaultPassword)) {
            throw new Exception("'SA_USERNAME' or 'SA_PASSWORD' should be filled in .env file.");
        }

        $defaultPassword = Hash::make($defaultPassword);

        $user = \App\User::where('email', $username)->first();
        if($user &&
            $defaultUsername === $username &&
            Hash::check($password, $defaultPassword)
        ) {
            Auth::login($user);
            return true;
        }
        return false;
    }
}
