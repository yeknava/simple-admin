<?php

use Yeknava\SimpleAdmin\Sample\SimpleAdminSampleService;
use Yeknava\SimpleAdmin\SimpleAdminBasicMiddleware;

return [
    /*
     * route's base path that will be used for package specific routes.
     * (use 'php artisan route:list' to find out all paths)
     */
    'route_base_path' => '/simple-admin',

    /*
     * middlewares that admin needs to pass before access to panel.
     * if you don't have any login process that admin may to use it to login first,
     * SimpleAdminBasicMiddleware will provide basics auth functionality. 
     */
    'middlewares' => [SimpleAdminBasicMiddleware::class, 'throttle:10,1'],

    /*
     * if you are using SimpleAdminBasicMiddleware middleware and you set
     * use_login_method config to false, you need to provide default
     * username and password to login. 
     */
    'default_username' => env('SA_USERNAME'),
    'default_password' => env('SA_PASSWORD'),

    /*
     * use admin service if you are using SimpleAdminBasicMiddleware
     */
    'admin_service' => SimpleAdminSampleService::class,

    /*
     * admin panel pages:
     */
    'pages' => [
        'users' => [
            'path' => '/users',
            'title' => 'Users',
            'create' => true,
            'edit' => true,
            'delete' => true,
            'view' => null,
            'force_create' => false,
            'force_update' => false,
            
            /*
             * content is model class, if you need customize any fields or
             * process you need to use SimpleAdminTrait in your model class
             */
            'content' => \App\User::class,
        ],
    ]
];