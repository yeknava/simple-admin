<?php

namespace Yeknava\SimpleAdmin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Throwable;

class Controller extends BaseController
{
    protected $current;
    protected $content;
    protected $pages;

    public function __construct() {
        $this->pages = config('simple-admin.pages');
        $currentPath = request()->getPathInfo();
        $basePath = config('simple-admin.route_base_path');
        $basePathCount = count(explode('/', $basePath));

        foreach($this->pages as $key => $page) {
            $configPath = $basePath.$page['path'];
            if(strtolower($configPath) === strtolower($currentPath)) {
                $this->content = new $page['content'];  
                $this->current = $page;
                $this->current['path_key'] = $key;
                break;
            } else {
                $arr = explode('/', $currentPath);
                if($arr && count($arr) > $basePathCount) {
                    $pathKey = $arr[$basePathCount];
                    if(strtolower($key) == strtolower($pathKey)) {
                        $this->content = new $page['content'];  
                        $this->current = $page;
                        $this->current['path_key'] = $key;
                        break;
                    }
                } else {
                    redirect(url('/').$configPath);
                }
            }
        }
    }

    public function list(Request $request) {
        $c = class_uses($this->content);
        $q = $request->query('q');
        if(isset($c['Yeknava\SimpleAdmin\SimpleAdminTrait'])) {
            $result = $this->content->simpleAdminList($q);
            $fields = $this->content->simpleAdminListFields();
        } else {
            $result = $this->content->paginate();
            $fields = $this->content->getConnection()->getSchemaBuilder()->getColumnListing($this->content->getTable());
        }

        $actions = [];
        if(isset($this->current['create'])) {
            $actions['create'] = 'create';
        }
        if(isset($this->current['edit'])) {
            $actions['edit'] = 'edit';
        }
        if(isset($this->current['delete'])) {
            $actions['delete'] = 'delete';
        }

        $view = $this->current['view'] ?? 'simpleadmin::simple_admin_list';

        return view($view)->with([
            'collection' => $result,
            'fields' => $fields,
            'actions' => $actions,
            'pages' => $this->pages,
            'current' => $this->current,
        ]);
    }

    public function create() {
        $c = class_uses($this->content);
        if(isset($c['Yeknava\SimpleAdmin\SimpleAdminTrait'])) {
            $fields = $this->content->simpleAdminStoreFields();
        } else {
            $fields = $this->content->getFillable();
        }

        return view('simpleadmin::simple_admin_form')->with([
            'fields' => $fields,
            'pages' => $this->pages,
            'current' => $this->current
        ]);
    }

    public function store(Request $request) {
        try {
            $c = class_uses($this->content);
    
            if(
                isset($c['Yeknava\SimpleAdmin\SimpleAdminTrait']) &&
                (!isset($this->current['force_create']) || $this->current['force_create'] === false)
            ) {
                $this->content->simpleAdminStore($request);
            } else {
                $this->content->create($request->all());
            }
        } catch (Throwable $e) {
            return back()->withInput()->withErrors(['error-message' => $e->getMessage()]);
        }

        return back();
    }

    public function edit(int $id) {
        $c = class_uses($this->content);
        $item = $this->content->findOrFail($id);
        if(isset($c['Yeknava\SimpleAdmin\SimpleAdminTrait'])) {
            $fields = $this->content->simpleAdminUpdateFields();
        } else {
            $fields = $this->content->getFillable();
        }

        return view('simpleadmin::simple_admin_form')->with([
            'item' => $item,
            'fields' => $fields,
            'pages' => $this->pages,
            'current' => $this->current
        ]);
    }

    public function update($id, Request $request) {
        $c = class_uses($this->content);

        if(
            isset($c['Yeknava\SimpleAdmin\SimpleAdminTrait']) &&
            (!isset($this->current['force_update']) || $this->current['force_update'] === false)
        ) {
            $this->content->simpleAdminUpdate($request, $id);
        } else {
            $item = $this->content->find($id);
            $item->fill($request->all())->save();
        }

        return back();
    }

    public function delete($id) {
        $c = class_uses($this->content);

        if(isset($c['Yeknava\SimpleAdmin\SimpleAdminTrait'])) {
            $this->content->simpleAdminDelete($id);
        } else {
            $this->content->delete($id);
        }

        return back();
    }
}
