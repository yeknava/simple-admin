@extends('simpleadmin::simple_admin_layout', ['pages'=>$pages])

@section('navbar-right')
    <a href="{{Request::url().'/new'}}"
       class="btn btn-primary">new</a>
@endsection

@section('content')
    <div class="container">
        <form method="post" action="{{
            isset($item) ? route('simple_admin_update_'.$current['path_key'], $item['id']) :
               route('simple_admin_store_'.$current['path_key']) }}">
            @if(isset($item))
                <input type="hidden" name="_method" value="PUT">
            @endif
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @foreach ($fields as $key => $field)
                @if(is_numeric($key))
                    <div class="form-group">
                        <label>{{$field}}</label>
                        <input
                            type="text"
                            name="{{$field}}"
                            value="{{isset($item[$field]) ? $item[$field] : ''}}"
                            class="form-control" />
                    </div>
                @elseif($field['type'] == 'text')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        <input name="{{$key}}" type="text"
                            value="{{isset($item[$key]) ? $item[$key] : ''}}"
                            class="form-control" />
                    </div>
                @elseif($field['type'] == 'password')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        <input name="{{$key}}" type="password"
                            value="{{isset($item[$key]) ? $item[$key] : ''}}"
                            class="form-control" />
                    </div>
                @elseif($field['type'] == 'number')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        <input name="{{$key}}" type="number"
                            value="{{isset($item[$key]) ? $item[$key] : ''}}"
                            class="form-control" />
                    </div>
                @elseif($field['type'] == 'email')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        <input name="{{$key}}" type="email"
                            value="{{isset($item[$key]) ? $item[$key] : ''}}"
                            class="form-control" />
                    </div>
                @elseif($field['type'] == 'file')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        <input name="{{$key}}" type="file"
                            value="{{isset($item[$key]) ? $item[$key] : ''}}"
                            class="form-control" />
                    </div>
                @elseif($field['type'] == 'date')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        <input name="{{$key}}" type="date"
                            value="{{isset($item[$key]) ? $item[$key] : ''}}"
                            class="form-control" />
                    </div>
                @elseif($field['type'] == 'hidden')
                    <input name="{{$key}}" type="hidden"
                        value="{{isset($item[$key]) ? $item[$key] : ''}}"
                        class="form-control" />
                @elseif($field['type'] == 'color')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        <input name="{{$key}}" type="color"
                            value="{{isset($item[$key]) ? $item[$key] : ''}}"
                            class="form-control" />
                    </div>
                @elseif($field['type'] == 'select')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        <select name="{{$key}}" class="custom-select">
                            @foreach ($field['options'] as $value => $option)
                                <option value="{{$value}}"
                                    @if(isset($item[$key]) && $item[$key] == $value)
                                        selected
                                    @elseif(!empty($field['default']) && $field['default'] == $value)
                                        selected
                                    @endif
                                    >
                                        {{$option}}
                                    </option>
                            @endforeach
                        </select>
                    </div>
                @elseif($field['type'] == 'checkbox')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        @foreach ($field['options'] as $value => $option)
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="{{$field['name']}}" id="checkbox-{{$value}}" value="{{$value}}"
                                    @if(isset($item[$field['name']]) && $item[$field['name']] == $value)
                                        checked
                                    @elseif(!empty($field['default']) && $field['default'] == $value)
                                        checked
                                    @endif
                                >
                                <label class="form-check-label" for="checkbox-{{$value}}">
                                    {{$option}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                @elseif($field['type'] == 'radio')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        @foreach ($field['options'] as $value => $option)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="{{$field['name']}}" id="radio-{{$value}}" value="{{$value}}"
                                    @if(isset($item[$field['name']]) && $item[$field['name']] == $value)
                                        checked
                                    @elseif(!empty($field['default']) && $field['default'] == $value)
                                        checked
                                    @endif
                                >
                                <label class="form-check-label" for="radio-{{$value}}">
                                    {{$option}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                @elseif($field['type'] == 'textarea')
                    <div class="form-group">
                        <label>{{$field['label']}}</label>
                        <textarea name="{{$key}}">
                            {{isset($item[$key]) ? $item[$key] : ''}}
                        </textarea>
                    </div>
                @endif
            @endforeach
            <div>
                <button class="btn btn-primary" type="submit">submit</button>
            </div>
        </form>
    </div>
@endsection