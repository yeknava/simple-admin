@extends('simpleadmin::simple_admin_layout', ['pages'=>$pages])

@section('navbar-right')
    @if(isset($current['create']) && $current['create'])
    <a href="{{Request::url().'/new'}}"
       class="btn btn-primary">new</a>
    @endif
@endsection

@section('content')
    <div class="container-fluid">
        @if(count($collection))
        <table class="table table-responsive table-striped">
            <thead>
                <tr>
                    @foreach ($fields as $key)
                        <th>{{$key}}</th>
                    @endforeach
                    @if(count($actions))
                        <th>actions</th>
                    @endif
                <tr>
            </thead>
            <tbody>
                @php
                    $itemCustomActions = [];
                    $masks = [];
                @endphp
                @foreach ($collection as $itemKey => $item)
                    @if(method_exists($item, 'simpleAdminListOptions'))
                        @php
                            $itemCustomActions[$itemKey] = $item->simpleAdminListOptions();
                            $masks = $item->simpleAdminMasks();
                        @endphp
                    @endif
                    <tr>
                        @foreach ($fields as $key)
                            @if($masks && isset($masks[$key]) && isset($masks[$key][$item[$key]]))
                                <td>{{$masks[$key][$item[$key]]}}</td>
                            @elseif(is_array($item[$key]))
                                @php
                                    $itemArrayKeys = array_keys($item[$key]);
                                    $itemArrayValues = array_values($item[$key]);
                                @endphp
                                <td>
                                    @for ($i = 0; $i < count($item[$key]); $i++)
                                        @if($i>0) , @endif
                                        {{$itemArrayKeys[$i]}}: {{$itemArrayValues[$i]}}
                                    @endfor
                                </td>
                            @else
                                <td>{{$item[$key]}}</td>
                            @endif
                        @endforeach
                        @if(count($actions))
                            <td style="display: flex;">
                                @if($actions['edit'])
                                    <a
                                        href="{{Request::url().'/'.$item['id'].'/edit'}}"
                                        class="btn btn-sm btn-info">{{$actions['edit']}}
                                    </a>
                                @endif
                                @if($actions['delete'])
                                    <form method="post" action="{{Request::url().'/'.$item['id'].''}}"
                                        onsubmit="return confirm('Do you really want to delete this item?');">
                                        <input type="hidden" name="_method" value="delete">
                                        <button type="submit" class="btn btn-sm btn-danger">{{$actions['delete']}}</button>
                                    </form>
                                @endif
                                @if(count($itemCustomActions) > 0 && isset($itemCustomActions[$itemKey]))
                                    @foreach ($itemCustomActions[$itemKey] as $key => $action)
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#action_{{$key}}">
                                            {{$action['title']}}
                                        </button>
                                    @endforeach
                                @endif
                            <td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$collection->links()}}
        @else
            <div>
                <h2>Empty list</h2>
            </div>
        @endif
    </div>
  
    {{-- TODO::Add foreach items --}}
    @foreach ($collection as $itemKey => $item)
        @if(count($itemCustomActions) > 0 && isset($itemCustomActions[$itemKey]))
            @foreach ($itemCustomActions[$itemKey] as $key => $action)
                <div class="modal fade" id="action_{{$key}}" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{$action['title']}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            @include('simpleadmin::simple_admin_custom_form', [
                                'fields' => $action['form']->fields,
                                'method' => $action['form']->method,
                                'action' => $action['form']->action,
                                'submitText' => $action['form']->submitText,
                                'item' => $item
                            ])
                        </div>
                    </div>
                    </div>
                </div>
            @endforeach
        @endif
    @endforeach
@endsection