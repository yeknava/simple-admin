<div class="container">
    <form method="post" action="{{$action}}">
        @if($method == "put")
            <input type="hidden" name="_method" value="PUT">
        @endif
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @foreach ($fields as $field)
            @if($field->type == "text")
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    <input
                        type="text"
                        name="{{$field->name}}"
                        value="{{isset($item[$field->name]) ? $item[$field->name] : $field->default ?? ''}}"
                        class="form-control" />
                </div>
            @elseif($field->type == "number")
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    <input
                        type="number"
                        name="{{$field->name}}"
                        value="{{isset($item[$field->name]) ? $item[$field->name] : $field->default ?? ''}}"
                        class="form-control" />
                </div>
            @elseif($field->type == "password")
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    <input
                        type="password"
                        name="{{$field->name}}"
                        value="{{isset($item[$field->name]) ? $item[$field->name] : $field->default ?? ''}}"
                        class="form-control" />
                </div>
            @elseif($field->type == "email")
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    <input
                        type="email"
                        name="{{$field->name}}"
                        value="{{isset($item[$field->name]) ? $item[$field->name] : $field->default ?? ''}}"
                        class="form-control" />
                </div>
            @elseif($field->type == "date")
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    <input
                        type="date"
                        name="{{$field->name}}"
                        value="{{isset($item[$field->name]) ? $item[$field->name] : $field->default ?? ''}}"
                        class="form-control" />
                </div>
            @elseif($field->type == "color")
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    <input
                        type="color"
                        name="{{$field->name}}"
                        value="{{isset($item[$field->name]) ? $item[$field->name] : $field->default ?? ''}}"
                        class="form-control" />
                </div>
            @elseif($field->type == "file")
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    <input
                        type="file"
                        name="{{$field->name}}"
                        value="{{isset($item[$field->name]) ? $item[$field->name] : $field->default ?? ''}}"
                        class="form-control" />
                </div>
            @elseif($field->type == "hidden")
                <div class="form-group">
                    <input
                        type="hidden"
                        name="{{$field->name}}"
                        value="{{isset($item[$field->name]) ? $item[$field->name] : $field->default ?? ''}}"
                        class="form-control" />
                </div>
            @elseif($field->type == 'select')
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    <select name="{{$field->name}}" class="custom-select">
                        @foreach ($field->options as $value => $option)
                            <option value="{{$value}}"
                                @if(isset($item[$field->name]) && $item[$field->name] == $value)
                                    selected="selected"
                                @elseif(!empty($field->default) && $field->default == $value)
                                    selected="selected"
                                @endif
                                >
                                    {{$option}}
                            </option>
                        @endforeach
                    </select>
                </div>
            @elseif($field->type == 'checkbox')
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    @foreach ($field->options as $value => $option)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="{{$field->name}}" id="checkbox-{{$value}}" value="{{$value}}"
                                @if(isset($item[$field->name]) && $item[$field->name] == $value)
                                    checked
                                @elseif(!empty($field->default) && $field->default == $value)
                                    checked
                                @endif
                            >
                            <label class="form-check-label" for="checkbox-{{$value}}">
                                {{$option}}
                            </label>
                        </div>
                    @endforeach
                </div>
            @elseif($field->type == 'radio')
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    @foreach ($field->options as $value => $option)
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="{{$field->name}}" id="radio-{{$value}}" value="{{$value}}"
                                @if(isset($item[$field->name]) && $item[$field->name] == $value)
                                    checked
                                @elseif(!empty($field->default) && $field->default == $value)
                                    checked
                                @endif
                            >
                            <label class="form-check-label" for="radio-{{$value}}">
                                {{$option}}
                            </label>
                        </div>
                    @endforeach
                </div>
            @elseif($field->type == 'textarea')
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    <textarea name="{{$field->name}}">
                        {{isset($item[$field->name]) ? $item[$field->name] : $field->default}}
                    </textarea>
                </div>
            @endif
        @endforeach
        <div>
            <button class="btn btn-primary" type="submit">
                {{$submitText ?? 'submit'}}
            </button>
        </div>
    </form>
</div>
