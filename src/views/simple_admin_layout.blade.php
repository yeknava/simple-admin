<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Yeknava">
    <title>Simple Admin</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('simple-admin-assets/bootstrap-4.3.1-dist/css/bootstrap.min.css')}}">
    <script src={{asset('simple-admin-assets/bootstrap-4.3.1-dist/js/jquery.min.js')}}></script>
    <script src={{asset('simple-admin-assets/bootstrap-4.3.1-dist/js/bootstrap.min.js')}}></script>

    <style>
      .content {
        margin-top: 80px;
      }

      @media (min-width: 768px) {
      }
    </style>
    <!-- Custom styles for this template -->
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="#">{{env('APP_NAME')}}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav mr-auto">
              @foreach($pages as $key => $page)
                @if(trim(Request::path(), '/') == trim(config('simple-admin.route_base_path').$page['path'], '/'))
                  <li class="nav-item active">
                  @else
                  <li class="nav-item">
                @endif
                  <a class="nav-link" href="{{url('/').'/'.trim(config('simple-admin.route_base_path').$page['path'], '/')}}">{{$page['title']}} @if(trim(Request::path(), '/') == trim(config('simple-admin.route_base_path').$page['path'], '/'))<span class="sr-only">(current)</span>@endif</a>
                </li>
              @endforeach
            </ul>
            <form class="form-inline my-2 my-lg-0" method="get" action="">
            <input class="form-control mr-sm-2" name="q" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </form>
            <div>
              @yield('navbar-right')
            </div>
        </div>
        </nav>
        <main role="main" class="container-fluid content">
          @if(!empty($errors))
            @foreach ($errors->all() as $error)
              <div class="alert alert-danger" role="alert">
                  {{$error}}
              </div>
            @endforeach
          @endif

          @yield('content')

        </main><!-- /.container -->        
    </body>
</html>
