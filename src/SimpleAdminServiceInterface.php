<?php

namespace Yeknava\SimpleAdmin;

interface SimpleAdminServiceInterface
{
    public function login(string $username, string $password) : bool;
}