<?php

namespace Yeknava\SimpleAdmin;

use Illuminate\Http\Request;

trait SimpleAdminTrait {

    public static function simpleAdminMasks() : array {
        return [
            // 'field_key' => [
            //        'key' => 'value'
            // ]
        ];
    }

    public function simpleAdminStoreFields() : array {
        $arr = [];
        foreach($this->getFillable() as $f) {
            $arr[$f] = [
                'label' => $f,
                'type' => 'text', //text, textarea, select, checkbox, radio
                'options' => [], //1 => 'first'
                'default' => '',
                'rules' => $this->simpleAdminStoreRules()[$f] ?? null
            ];
        }

        return $arr;
    }

    public function simpleAdminUpdateFields() : array {
        $arr = [];
        foreach($this->getFillable() as $f) {
            $arr[$f] = [
                'label' => $f,
                'type' => 'text', //text, textarea, select
                'options' => [],
                'default' => '',
                'rules' => $this->simpleAdminUpdateRules()[$f] ?? null
            ];
        }

        return $arr;
    }

    public function simpleAdminSupport() : array {
        return [
            'list', 'create', 'update', 'delete'
        ];
    }

    public function simpleAdminListFields() : array {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function simpleAdminSearchables() : ?array {
        return ['id'];
    }

    public function simpleAdminStoreRules() : array {
        return [
            'email' => ''
        ];
    }

    public function simpleAdminUpdateRules() : array {
        return [
            'email' => ''
        ];
    }

    public function simpleAdminList(?string $str = null) {
        $query = $this;
        if($str && !empty($this->simpleAdminSearchables())) {
            $i = 0;
            foreach($this->simpleAdminSearchables() as $searchable) {
                if($i++ == 0) $query = $query->where($searchable, $str);
                else $query = $query->orWhere($searchable, $str);
            }
        }
        return $query->orderBy('id', 'desc')->paginate(20);
    }

    public function simpleAdminStore(Request $request) {
        $data = $request->validate($this->simpleAdminStoreRules());
        $s = new static($data);
        $s->save();

        return $s;
    }

    public function simpleAdminUpdate(Request $request, $id) {
        $s = static::findOrFail($id);
        $data = $request->validate($this->simpleAdminUpdateRules());
        $s->fill($data)->save();

        return $s;
    }

    public function simpleAdminDelete($id) {
        $s = static::find($id);
        $s->delete();

        return true;
    }

    public function simpleAdminListOptions() : array {
        /* 
         * return [
         *      [
         *          'title' => 'title',
         *          'form'  => (new Form)
         *      ]
         * ];
         */  
        return [
        ];
    }
}