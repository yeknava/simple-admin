<?php

namespace Yeknava\SimpleAdmin;

class Field
{
    public $name;
    public $type;
    public $label;
    public $options;
    public $rules;
    public $default;

    public function __construct()
    {
        $this->type = "text";
        $this->label = null;
        $this->default = null;
        $this->options = [];
    }

    public function setName(string $name) {
        $this->name = $name;
        return $this;
    }

    public function setType(string $type) {
        $this->type = $type;
        return $this;
    }

    public function setLabel(string $label) {
        $this->label = $label;
        return $this;
    }

    public function setOptions(array $options) {
        $this->options = $options;
        return $this;
    }

    public function addOption(string $value, string $title = null) {
        $this->options[$value] = $title ?? $value;
        return $this;
    }

    public function setRules(string $rules) {
        $this->rules = $rules;
        return $this;
    }

    public function setDefault($default) {
        $this->default = $default;
        return $this;
    }
}
