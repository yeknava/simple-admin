<?php
use Yeknava\SimpleAdmin\Controller;

$path = config('simple-admin.route_base_path', '/');
$pages = config('simple-admin.pages', []);

Route::middleware(config('simple-admin.middlewares'))->group(function () use ($path, $pages) {

    foreach($pages as $key => $page) {
        Route::get($path.$page['path'], Controller::class.'@list')
            ->name('simple_admin_list_'.$key);
        Route::get($path.$page['path'].'/new', Controller::class.'@create')
            ->name('simple_admin_create_'.$key);
        Route::get($path.$page['path'].'/{id}/edit', Controller::class.'@edit')
            ->name('simple_admin_edit_'.$key);
        Route::post($path.$page['path'], Controller::class.'@store')
            ->name('simple_admin_store_'.$key);
        Route::put($path.$page['path'].'/{id}', Controller::class.'@update')
            ->name('simple_admin_update_'.$key);
        Route::delete($path.$page['path'].'/{id}', Controller::class.'@delete')
            ->name('simple_admin_delete_'.$key);
    }

    if (count($pages) > 0) {
        $firstPageKey = $pages[array_key_first($pages)] ?? null;
        if (!empty($firstPageKey) && !empty($firstPageKey['path'])) {
            Route::redirect($path, $path.$firstPageKey['path']);
        }
    }

});