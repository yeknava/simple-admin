<?php

namespace Yeknava\SimpleAdmin;

use Illuminate\Support\ServiceProvider;

class SimpleAdminServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/simple-admin.php' => $this->app->configPath().'/simple-admin.php',
            __DIR__ . '/../assets' => $this->app->publicPath().'/simple-admin-assets/',
        ]);

        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->loadViewsFrom(__DIR__.'/views', 'simpleadmin');
    }
}
