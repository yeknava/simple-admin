<?php

namespace Yeknava\SimpleAdmin;

class Form
{
    public $fields;
    public $method;
    public $action;
    public $submitText;

    public function __construct()
    {
        $this->fields = [];
    }

    public function addField(Field $field) {
        $this->fields[] = $field;
        return $this;
    }

    public function setMethod(string $value) {
        $this->method = $value;
        return $this;
    }

    public function setAction(string $value) {
        $this->action = $value;
        return $this;
    }

    public function setSubmitButtonText(string $text) {
        $this->submitText = $text;
        return $this;
    }

    public function toView() {
        return view('simpleadmin::simple_admin_custom_form')->with([
            'fields' => $this->fields,
            'method' => $this->method,
            'action' => $this->action,
            'submitText' => $this->submitText
        ]);
    }
}
