<?php

namespace Yeknava\SimpleAdmin;

use Closure;
use Exception;
use ReflectionClass;

class SimpleAdminBasicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('PHP_AUTH_USER')) {
            $username = $request->header('PHP_AUTH_USER');
            $password = $request->header('PHP_AUTH_PW');

            if(empty($username) || empty($password)) {
                return response('', 401, [
                    'WWW-Authenticate' => 'Basic realm=simpleadmin'
                ]);
            }

            $servicePath = config('simple-admin.admin_service');
            $reflectionClass = new ReflectionClass($servicePath);
            $service = $reflectionClass->newInstance();
            if($service instanceof SimpleAdminServiceInterface) {
                if($service->login($username, $password)) {
                    return $next($request);
                }
            }
        }
        return response('', 401, [
            'WWW-Authenticate' => 'Basic realm=simpleadmin'
        ]);
    }
}
